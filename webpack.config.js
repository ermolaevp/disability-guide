const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

function config(env) {
  return {
    entry: [
      './src/index.tsx',
    ],
    resolve: {
      extensions: ['.ts', '.tsx', '.js'],
      modules: ['node_modules']
    },
    module: {
      rules: [
        {
          test: /\.css$/,
          use: [ 'style-loader', 'postcss-loader' ],
        },
        {
          test: /\.tsx?$/,
          exclude: /(node_modules|bower_components)/,
          use: ['babel-loader', 'ts-loader']
        },
        {
          test: /\.(png|jpg|jpeg|gif)$/,
          use: 'file-loader',
        },
      ]
    },
    devServer: {
      contentBase: './dist',
      port: 8001,
      historyApiFallback: true,
      hot: true,
    },
    plugins: [
      new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
        template: 'index.tpl.html',
        chunksSortMode: 'dependency',
      }),
    ],
  };
}

module.exports = config;
