import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './app';
import smoothscroll from 'smoothscroll-polyfill';
import './assets/styles/index.css';

smoothscroll.polyfill();

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#236192',
      light: '#5a8ec3',
      dark: '#003764',
      contrastText: '#fff',
    },
    secondary: {
      main: '#37a000',
      light: '#6fd242',
      dark: '#007000',
      contrastText: '#fff',
    },
    text: {
      secondary: 'rgba(0, 0, 0, 0.38)',
    },
    background: {
      // default: '#fff',
    },
  },
  typography: {
    fontSize: 16,
    headline: {
      fontSize: 16,
      fontWeight: 500,
    },
    subheading: {
      fontSize: 15,
      fontWeight: 500,
    },
    body2: {
      fontSize: 12,
      lineHeight: '0.9rem',
    },
  },
  overrides: {
    MuiDivider: {
      root: {
        marginTop: '.5rem',
      },
    },
    MuiInput: {
      underline: {
        '&:before': {
          borderBottomColor: 'rgba(0, 0, 0, 0.12)' },
      },
    },
  },
});

const app = (
  <MuiThemeProvider theme={theme}>
    <App />
  </MuiThemeProvider>
);

ReactDOM.render(app, document.getElementById('root'));
