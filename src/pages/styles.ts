import { Theme } from '@material-ui/core/styles';

const styles = (theme: Theme) => ({
  root: {
    display: 'block',
    minWidth: 360,
    maxWidth: 700,
    backgroundColor: '#fff',
    margin: '0 auto',
    padding: '0 0 1rem 0',
  },
  form: {
  },
  formControl: {
    display: 'block',
    padding: '0 25px',
  },
  group: {
    margin: `${theme.spacing.unit}px 0`,
  },
  button: {
    fontSize: 18,
  },
  yellowDivider: {
    backgroundColor: '#FFBF3F',
    color: '#fff',
    display: 'flex',
    alignItems: 'center',
    padding: '0.2rem 25px',
    marginBottom: '1rem',
  },
  blueHeader: {
    backgroundColor: theme.palette.primary.main,
    padding: '1rem 25px',
    color: '#fff',
  },
  getStartedButton: {
    color: '#fff',
    border: '1px solid #fff',
    borderRadius: 20,
    margin: '0 auto',
    display: 'block',
    fontSize: 18,
    width: 238,
  },
  disclaimer: {
    color: theme.palette.text.secondary,
    padding: '2rem 25px',
  },
  copyRight: {
    color: theme.palette.text.secondary,
  },
  terms: {
    padding: '0 25px',
    color: theme.palette.text.secondary,
  },
  logos: {
    display: 'flex',
    justifyContent: 'space-evenly',
    padding: '2rem 0',
  },
});

export default styles;
