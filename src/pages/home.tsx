import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Select from '@material-ui/core/Select';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import { compose, withHandlers, withReducer } from 'recompose';
import * as actions from '../actions';
import Associations from '../components/associations';
import Header from '../components/header';
import HiddenFields from '../components/hidden-fields';
import InputField from '../components/input-field';
import InputWithMask from '../components/input-with-mask';
import McAfeeBBB from '../components/mcafee-bbb';
import RadioGroupInput from '../components/radio-group-input';
import VSpacer from '../components/v-spacer';
import { guideForm as guideFormReducer } from '../reducers';
import * as validators from '../validators';
import styles from './styles';

const { validate, isBlank, isNotEmail, isNotPhone, isNotZipCode } = validators;

const FORM_ACTION_URL = 'https://eligibility.disabilityguide.com/funnel';

const ages = () => {
  const ages1865 = [...Array(48).keys()].map((a: number) => a + 18);
  return [
    '17 and under',
    ...ages1865,
    '66+',
  ];
};

const smoothScroll = () => document.querySelector('#get-help-applying-for-disability').scrollIntoView({ behavior: 'smooth' });

const Home = ({
  guideForm,
  setFieldValue,
  classes,
  handleSubmit,
}: any) => (
  <div className={classes.root}>
    <Header />
    <div className={classes.blueHeader} id="get-help-applying-for-disability">
      <Typography color="inherit" variant="title">Get Help Applying For Disability</Typography>
      <VSpacer size={0.3} />
      <Typography color="inherit">You could qualify for up to $2,788/month. Fill out the quick form below to see if you qualify for help.</Typography>
    </div>
    <div className={classes.yellowDivider}>
      <Typography color="inherit" variant="headline">HEALTH & WORK HISTORY</Typography>
    </div>
    <form
      noValidate
      autoComplete="off"
      className={classes.form}
      method="POST"
      action={FORM_ACTION_URL}
      onSubmit={handleSubmit}
    >
      <RadioGroupInput
        title="Do you earn less than $1,000 a month?"
        name="earning_less_than_1000"
        value={guideForm.earning_less_than_1000}
        onChange={setFieldValue}
        error={guideForm.errors.earning_less_than_1000}
      />
      <RadioGroupInput
        title="Do you expect to be out of work for at least 12 months?"
        name="affected_work"
        value={guideForm.affected_work}
        onChange={setFieldValue}
        error={guideForm.errors.affected_work}
      />
      <RadioGroupInput
        title="Do you already receive Social Security Disability benefits?"
        name="receiving_benefits"
        value={guideForm.receiving_benefits}
        onChange={setFieldValue}
        error={guideForm.errors.receiving_benefits}
      />
      <RadioGroupInput
        title="Have you worked full-time for 5 out of the last 10 years?"
        name="worked_5_of_10"
        value={guideForm.worked_5_of_10}
        onChange={setFieldValue}
        error={guideForm.errors.worked_5_of_10}
      />
      <RadioGroupInput
        title="Are you seeing a doctor or taking any prescription medication?"
        name="current_dr_or_script"
        value={guideForm.current_dr_or_script}
        onChange={setFieldValue}
        error={guideForm.errors.current_dr_or_script}
      />
      <VSpacer />
      <div id="how-will-you-advocate" className={classes.yellowDivider}>
        <Typography color="inherit" variant="headline">HOW WILL YOUR ADVOCATE REACH YOU?</Typography>
      </div>
      <div className={classes.formControl}>
        <InputField
          name="first_name"
          label="First Name"
          onChange={setFieldValue}
          value={guideForm.first_name}
          error={guideForm.errors.first_name}
          fullWidth
        />
        <InputField
          name="last_name"
          label="Last Name"
          onChange={setFieldValue}
          value={guideForm.last_name}
          error={guideForm.errors.last_name}
          fullWidth
        />
        <InputField
          name="email_address"
          label="Email Address"
          onChange={setFieldValue}
          value={guideForm.email_address}
          error={guideForm.errors.email_address}
          type="email"
          fullWidth
        />
        <InputWithMask
          name="phone_home"
          label="Telephone Number"
          mask="phone"
          onChange={setFieldValue}
          value={guideForm.phone_home}
          type="tel"
          error={guideForm.errors.phone_home}
          fullWidth
        />
        <InputWithMask
          name="zip_code"
          label="Zip Code (Postal Code)"
          mask="zipCode"
          type="tel"
          onChange={setFieldValue}
          value={guideForm.zip_code}
          error={guideForm.errors.zip_code}
          fullWidth
        />
        <FormControl
          error={!!guideForm.errors.age}
          fullWidth
          margin="normal"
        >
          <InputLabel htmlFor="age">Age{guideForm.errors.age ? ' ' + guideForm.errors.age : ''}</InputLabel>
          <Select
            name="age"
            onChange={setFieldValue}
            value={guideForm.age}
            fullWidth
          >
            {ages().map((age: number | string) => (
              <MenuItem key={age} value={age}>
                {age}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>
      <VSpacer />
      <FormControl className={classes.formControl} margin="normal">
        <Button
          variant="raised"
          color="secondary"
          size="large"
          className={classes.button}
          type="submit"
        >
            see if I qualify
        </Button>
      </FormControl>
      <HiddenFields />
    </form>
    <McAfeeBBB />
    <div className={classes.terms}>
      <Typography color="inherit" variant="body2">
        By submitting this form, I expressly give consent to Adduco Media as well as Disability Alliance, a sponsoring advocate, or a sponsoring attorney to contact me using emails, SMS, phone calls and prerecorded messages at any phone number I provide, even if the number is a wireless number or on any federal or state do-not-call list. I understand that calls may be placed using automated technology, that I am not required to submit this form to apply for Social Security Disability Benefits, and that consent is not a requirement of purchase.
      </Typography>
    </div>
    <Associations />
    <div className={classes.blueHeader}>
      <VSpacer />
      <Typography color="inherit" variant="subheading">OUT OF WORK DUE TO A HEALTH ISSUE?</Typography>
      <VSpacer size={0.3} />
      <Typography color="inherit">Our advocates have helped thousands of people just like you through the disability approval process. You may qualify for up to $2,788/month in benefits.</Typography>
      <VSpacer size={1} />
      <Button
        className={classes.getStartedButton}
        color="primary"
        onClick={smoothScroll}
      >
        Get Started!
      </Button>
      <VSpacer />
    </div>
    <div className={classes.disclaimer}>
      <Typography color="inherit" variant="body2">
        DISCLAIMER: This is a paid attorney/advocate advertisement. This website is a group advertisement and a fee is paid for by participating attorneys and advocates. The site is not an attorney referral service or prepaid legal services plan. The site is privately owned and is not affiliated with or endorsed by the Social Security Administration, Department of Education, or any other government agency. The promotion of this website is sponsored exclusively by Attorneys and Advocacy Groups who provide services applicable to this website for the public. Any information you submit to this website may not be protected by attorney-client privilege. An automated matching system will match each request with a member attorney/advocate representing the specific geography. A list of participating attorneys is available at your request, to request this list free of charge please click here and fill out the following form. To see a list of advertisers in New York, click here. To see a list of advertisers in South Carolina, click here. This website, it’s owners, affiliates, and partners do not claim to be affiliated with any Local, State, or Federal Government agencies, and our advertising materials, or methods are not affiliated or approved by the U.S. Government. This website assists people in obtaining services applicable to the content of this website by pre-qualifying our clients. We then match our clients with the applicable service provider or company within our company’s network of attorneys, and advocates.
      </Typography>
    </div>
    <div className={classes.copyRight}>
      <Typography color="inherit" align="center" variant="body2">Disability Guide © Copyright 2018</Typography>
      <Typography color="inherit" align="center" variant="body2">Privacy Policy | Terms of Use</Typography>
    </div>
  </div>
);

const guideFormInitalState = {
  earning_less_than_1000: '',
  affected_work: '',
  receiving_benefits: '',
  worked_5_of_10: '',
  current_dr_or_script: '',
  first_name: '',
  last_name: '',
  email_address: '',
  phone_home: '',
  zip_code: '',
  age: '',
  errors: {},
};

export default compose(
  withStyles(styles),
  withReducer('guideForm', 'dispatch', guideFormReducer, guideFormInitalState),
  withHandlers({
    setFieldValue: ({ dispatch }) => (e: any) => {
      dispatch(actions.setFieldValue({ field: e.target.name, value: e.target.value }));
    },
    handleSubmit: ({ dispatch, guideForm }) => (e: any) => {
      e.preventDefault();
      const errors = {...guideForm.errors};
      errors.earning_less_than_1000 = validate(guideForm.earning_less_than_1000, [isBlank]);
      errors.affected_work = validate(guideForm.affected_work, [isBlank]);
      errors.receiving_benefits = validate(guideForm.receiving_benefits, [isBlank]);
      errors.worked_5_of_10 = validate(guideForm.worked_5_of_10, [isBlank]);
      errors.current_dr_or_script = validate(guideForm.current_dr_or_script, [isBlank]);
      errors.first_name = validate(guideForm.first_name, [isBlank]);
      errors.last_name = validate(guideForm.first_name, [isBlank]);
      errors.email_address = validate(guideForm.email_address, [isBlank, isNotEmail]);
      errors.phone_home = validate(guideForm.phone_home, [isBlank, isNotPhone]);
      errors.zip_code = validate(guideForm.zip_code, [isBlank, isNotZipCode]);
      errors.age = validate(guideForm.age, [isBlank]);
      dispatch(actions.setFormErrors(errors));
      let hasErrors = false;
      for (const field in errors) {
        if (errors.hasOwnProperty(field) && typeof errors[field] === 'string') {
          hasErrors = true;
          break;
        }
      }
      if (!hasErrors) {
        e.target.submit();
      } else {
        // tslint:disable-next-line
        console.error('Form errors:', errors);
      }
    },
  }),
)(Home);
