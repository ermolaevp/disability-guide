import * as actions from '../actions';

export const guideForm = (state: any, action: any) => {
  if (action.type === actions.SET_FIELD_VALUE) {
    return {
      ...state,
      [action.payload.field]: action.payload.value,
      errors: {
        ...state.errors,
        [action.payload.field]: null,
      },
    };
  }
  if (action.type === actions.SET_FIELD_ERROR) {
    return {
      ...state,
      errors: {
        ...state.errors,
        [action.payload.field]: action.payload.error,
      },
    };
  }
  if (action.type === actions.SET_FORM_ERRORS) {
    return {
      ...state,
      errors: {
        ...action.payload,
      },
    };
  }
  return state;
};
