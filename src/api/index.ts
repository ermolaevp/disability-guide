export const submitForm = (url: string, data: any) => {
  return fetch(url, {
    method: 'POST',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
    },
  }).then((res: any) => res.json())
    // tslint:disable-next-line
    .catch((error: any) => console.error('Error:', error))
    // tslint:disable-next-line
    .then((response: any) => console.log('Success:', response));
};
