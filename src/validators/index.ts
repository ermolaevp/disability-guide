const emailRegexp = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
const zipCodeRegexp = /\d{5}/;

export const isBlank = (
  value: string,
  message: string = 'is required.',
) => value === '' ? message : false;

export const isNotEmail = (
  value: string,
  message: string = 'is invalid.',
) => !emailRegexp.test(value) ? message : false;

export const isNotPhone = (
  value: string,
  message: string = 'is invalid.',
) => {
  value = value.replace(/[^\d+]/g, '');
  if (value.length < 11) { value = '1' + value; }
  return value.length !== 11 ? message : false;
};

export const isNotZipCode = (
  value: string,
  message: string = 'is invalid.',
) => !zipCodeRegexp.test(value) ? message : false;

export const validate = (value: string, validators: any[]) => {
  for (const validator of validators) {
    const message = validator(value);
    if (message) { return message; }
  }
};
