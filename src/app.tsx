import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import Content from './components/content';
import Home from './pages/home';

const App = () => (
  <div>
    <CssBaseline />
    <Content>
      <Home />
    </Content>
  </div>
);

export default App;
