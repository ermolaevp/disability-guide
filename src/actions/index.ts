export const SET_FIELD_VALUE = 'SET_FIELD_VALUE';
export const SET_FIELD_ERROR = 'SET_FIELD_ERROR';
export const SET_FORM_ERRORS = 'SET_FORM_ERRORS';

export const setFieldValue = (payload: {field: string, value: string}) => ({
  type: SET_FIELD_VALUE,
  payload,
});

export const setFieldError = (payload: {field: string, error: string}) => ({
  type: SET_FIELD_ERROR,
  payload,
});

export const setFormErrors = (payload: any) => ({
  type: SET_FORM_ERRORS,
  payload,
});
