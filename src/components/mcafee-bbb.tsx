import * as React from 'react';
import styled from 'styled-components';

const mcAfee = require('../assets/images/mcafee.png');
const mcAfee2x = require('../assets/images/mcafee@2x.jpg');
const bbb = require('../assets/images/bbb.png');
const bbb2x = require('../assets/images/bbb@2x.png');

const McAfeeBBB = () => (
  <StyledLogosWrapper>
    <StyledLogos>
      <StyledMcAfee />
      <StyledBBB />
    </StyledLogos>
  </StyledLogosWrapper>
);

const StyledLogosWrapper = styled.div`
  padding: 2rem 25px;
`;
const StyledLogos = styled.div`
  height: 45px;
  width: 100%;
  display: flex;
  justify-content: space-evenly;
  @media (min-width: 700px) {
    height: 90px;
  }
`;
const StyledMcAfee = styled.div`
  width: 228px;
  background: no-repeat url(${mcAfee2x}) center center;
  @media (max-width: 700px) {
    background: no-repeat url(${mcAfee}) center center;
    width: 114px;
  }
`;
const StyledBBB = styled.div`
  width: 333px;
  background: no-repeat url(${bbb2x}) center center;
  @media (max-width: 700px) {
    background: no-repeat url(${bbb}) center center;
    width: 167px;
  }
`;

export default McAfeeBBB;
