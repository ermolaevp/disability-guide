import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import { Theme, withStyles } from '@material-ui/core/styles';
import * as React from 'react';

const styles = (theme: Theme) => ({
  formControl: {
    display: 'block',
  },
});

interface IInputField {
  name: string;
  label: string;
  onChange: any;
  value: string;
  error: string | null | undefined;
  classes: any;
  type?: string;
  fullWidth?: boolean;
  required?: boolean;
}

const InputField = ({
  name,
  label,
  onChange,
  value,
  error,
  classes,
  type = 'text',
  fullWidth = false,
  required = false,
}: IInputField) => (
  <FormControl
    className={classes.formControl}
    error={!!error}
    fullWidth={fullWidth}
    required={required}
    margin="normal"
  >
    <InputLabel htmlFor={name}>
      {error ? `${label} ${error}` : label}
    </InputLabel>
    <Input
      id={name}
      name={name}
      value={value}
      type={type}
      onChange={onChange}
      fullWidth={fullWidth}
    />
  </FormControl>
);

export default withStyles(styles)(InputField);
