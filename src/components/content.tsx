import * as React from 'react';

const Content = (props: any) => (
  <div style={{ padding: '0' }}>
    {props.children}
  </div>
);

export default Content;
