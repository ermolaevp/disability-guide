import Divider from '@material-ui/core/Divider';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import { Theme, withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';

const styles = (theme: Theme) => ({
  formControl: {
    display: 'block',
    padding: '0 25px',
  },
  group: {
    margin: `${theme.spacing.unit}px 0`,
  },
});

interface IRadioGroupInput {
  name: string;
  title: string;
  value: string;
  error: string | undefined | null;
  onChange: any;
  classes: any;
}

const RadioGroupInput = ({
  name,
  title,
  value,
  error,
  onChange,
  classes,
}: IRadioGroupInput) => (
    <FormControl error={!!error} className={classes.formControl} margin="normal">
      <Typography variant="subheading">{title}</Typography>
      <Divider />
      <RadioGroup
        aria-label={name}
        name={name}
        value={value}
        onChange={onChange}
        className={classes.group}
      >
        <FormControlLabel value="yes" control={<Radio />} label="Yes" />
        <FormControlLabel value="no" control={<Radio />} label="No" />
      </RadioGroup>
      {error && <FormHelperText>You need to choose something.</FormHelperText>}
    </FormControl>
);

export default withStyles(styles)(RadioGroupInput);
