import * as React from 'react';

interface IVSpacerParams {
  size?: number;
  unit?: string;
}

const VSpacer = ({ size = 1, unit = 'rem' }: IVSpacerParams) => (
  <div
    style={{
      height: 1,
      width: '100%',
      marginBottom: `${size}${unit}`,
    }}
  />
);

export default VSpacer;
