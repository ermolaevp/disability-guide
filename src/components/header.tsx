import * as React from 'React';
import styled from 'styled-components';

const logo = require('../assets/images/logo.png');
const logo2x = require('../assets/images/logo@2x.png');
const workers = require('../assets/images/workers.png');
const workers2x = require('../assets/images/workers@2x.png');

const Header = () => (
  <StyledHeader>
    <StyledLogo />
    <StyledWorkers />
  </StyledHeader>
);

const StyledHeader = styled.header`
  padding: 0 25px;
  height: 102px;
  display: flex;
  justify-content: space-between;
  flex-wrap: nowrap;
  @media (max-width: 700px) {
    height: 51px;
  }
`;

const StyledLogo = styled.div`
  background: no-repeat url(${logo2x}) 0 50%;
  width: 262px;
  @media (max-width: 700px) {
    background: no-repeat url(${logo}) 0 50%;
    width: 131px;
  }
`;

const StyledWorkers = styled.div`
  background: no-repeat url(${workers2x}) 100% -23%;
  width: 362px;
  @media (max-width: 700px) {
    background: no-repeat url(${workers}) 100% -23%;
    width: 181px;
  }
`;

export default Header;
