import * as React from 'react';
import styled from 'styled-components';

const associationsLogo = require('../assets/images/associations.png');
const associationsLogo2x = require('../assets/images/associations@2x.png');

const Associations = () => (
  <StyledAssociationsWrapper>
    <StyledAssociations />
  </StyledAssociationsWrapper>
);

const StyledAssociationsWrapper = styled.div`
  padding: 2rem 25px;
`;

const StyledAssociations = styled.div`
    background: no-repeat url(${associationsLogo2x}) center center;
    width: 644px;
    height: 86px;
    margin: 0 auto;
    @media (max-width: 700px) {
      background: no-repeat url(${associationsLogo}) center center;
      width: 322px;
      height: 43px;
    }
`;

export default Associations;
