import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import * as React from 'react';
import MaskedInput from 'react-text-mask';

const MaskedPhone = ({ inputRef, ...props }: any) => (
  <MaskedInput
    {...props}
    ref={inputRef}
    mask={['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, ' ', '-', ' ', /\d/, /\d/, /\d/, /\d/]}
    placeholderChar={'\u2000'}
    // showMask
  />
);

const MaskedZipCode = ({ inputRef, ...props }: any) => (
  <MaskedInput
    {...props}
    ref={inputRef}
    mask={[/\d/, /\d/, /\d/, /\d/, /\d/]}
  />
);

const maskedInputs = {
  phone: MaskedPhone,
  zipCode: MaskedZipCode,
};

interface IInputMask {
  name: string;
  label: string;
  onChange: any;
  value: string;
  error: string | undefined | null;
  mask: 'phone' | 'zipCode';
  type?: string;
  fullWidth?: boolean;
  required?: boolean;
}

const InputWithMask = ({
  name,
  label,
  onChange,
  value,
  error,
  mask,
  type = 'text',
  fullWidth = false,
  required = false,
}: IInputMask) => (
    <FormControl
      error={!!error}
      fullWidth={fullWidth}
      required={required}
      margin="normal"
    >
      <InputLabel htmlFor={name}>{error ? `${label} ${error}` : label}</InputLabel>
      <Input
        id={name}
        name={name}
        value={value}
        type={type}
        onChange={onChange}
        inputComponent={maskedInputs[mask]}
      />
    </FormControl>
);

export default InputWithMask;
