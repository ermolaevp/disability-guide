import * as React from 'react';

const hfs = [
  { name: 'ckm_campaign_id', value: '7' },
  { name: 'ckm_key', value: '2YVjtyaLrOY' },
  { name: 'src', value: 'gads' },
  { name: 'utm_source', value:  '' },
  { name: 'utm_medium', value:  '' },
  { name: 'utm_campaign', value:  '' },
  { name: 'utm_term', value:  '' },
  { name: 'utm_content', value:  '' },
  { name: 'matchtype', value:  '' },
  { name: 'device', value:  '' },
  { name: 'campaignid', value:  '' },
  { name: 'adgroupid', value:  '' },
  { name: 'placement', value:  '' },
  { name: 'network', value:  '' },
  { name: 'creative', value:  '' },
  { name: 'gclid', value:  '' },
  { name: 'text_msg', value: 'true' },
  { name: 'ckm_subid', value: 'Google' },
  { name: 'xxtrustedformtoken', value:  '' },
  { name: 'xxtrustedformcerturl', value:  '' },
  { name: 'logic_group_id', value: '2'},
  { name: 'referrer', value:  '' },
];

const HiddenFields = () => (
  <div>
    {hfs.map((hf) => (
      <input key={hf.name} name={hf.name} value={hf.value} type="hidden" />
    ))}
  </div>
);

export default HiddenFields;
